from turtle import *

speed(0)
MOVE_DIST = 20

# Draw the beach
bgcolor("#D2691E")

# Draw the ocean
penup()
goto(100,400)
pendown()
color("blue")
begin_fill()
goto(600,400)
goto(600,-400)
goto(100,-400)
goto(100,400)
end_fill()

# Draw the turtle
penup()
goto(-200,0)
shape("turtle")
color("green")

# Functions to move the turtle
def move_up():
    setheading(90)
    forward(MOVE_DIST)
    check_goal()

def move_down():
    setheading(270)
    forward(MOVE_DIST)
    check_goal()

def move_left():
    setheading(180)
    forward(MOVE_DIST)    
    check_goal()

def move_right():
    setheading(0)
    forward(MOVE_DIST)
    check_goal()

def check_goal():
    if xcor()>100:
        hideturtle()
        color("white")
        write("You Win!")
        onkey(None,"Up")
        onkey(None,"Down")
        onkey(None,"Left")
        onkey(None,"Right")


onkey(move_up,"Up")
onkey(move_down,"Down")
onkey(move_left,"Left")
onkey(move_right,"Right")

listen()
done()